package com.example.aravindb.tcppatientv4;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.Socket;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.mmm.healthcare.scope.AudioInputSwitch;
import com.mmm.healthcare.scope.AudioOutputSwitch;
import com.mmm.healthcare.scope.ConfigurationFactory;
import com.mmm.healthcare.scope.Errors;
import com.mmm.healthcare.scope.HeadsetAudioSource;
import com.mmm.healthcare.scope.IBluetoothManager;
import com.mmm.healthcare.scope.IStethoscopeListener;
import com.mmm.healthcare.scope.Stethoscope;

import static android.R.id.message;
import static java.lang.Thread.sleep;

public class MainActivity extends AppCompatActivity {




    private int Max_Consultation_Time_in_mins = 10;
    private String ServerIP = "epionex.com";
    //private String ServerIP = "192.168.31.200";
    //testing
    
    private int ServerSocket = 1235;
    private String ServerJarName = "trial";
    private String TomCatServerURL ="http://103.35.198.49:8080/VitalServerTCP/starttcpserver";
    private String stethsn="NA";
    public static boolean HR_THREAD_FLAG = false;

    Dialog modedialog;
    TextView clientmode;
    LineChart stethgraph;
    boolean LOCAL_REC = false;
    public static long StartTime;
    public static Stethoscope stethoscope=null;
    public static byte[] buffer;

    public static float[] aus_data_y;//this array will reset itself
    public static int aus_data_y_index;
    public static int aus_data_y_index_rear;
    public static int aus_data_x_index = 0;

    private Thread hrThread;
    private Thread drThread;
    boolean ExecutionStopRequired;
    public static boolean THREAD_START;
    public static int maxSize;
    public static int front;
    public static int rear;
    public static byte[][] cir_buf;
    XAxis xl;

    public static Integer samples_uploaded;
    public static Integer samples_read;
    private static Integer upload_err;
    public static Integer stethreaderr;

    public static Socket soc;

    public static ArrayList<String> list_displaylog;
    public static ArrayAdapter logarrayAdapter;

    Thread connect_to_steth;
    SimpleDateFormat df;
    ProgressDialog progressDialog;

    public void start_connect()
    {
        //simpleProgressBar.setVisibility(View.VISIBLE);
        //setProgressValue(20);
        //printConsole("Connecting to Stethoscope...");
        //change_StethStatusLED(0);
        connect_to_steth = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    InputStream is = getAssets().open("license.xml");
                    ConfigurationFactory.setLicenseFile(is);
                } catch (Exception e) {
                    e.printStackTrace();
                    //Toast.makeText(this, "License Error", Toast.LENGTH_LONG).show();
                    printConsole("License Error");
                    ExecutionStopRequired = true;
                    //change_StethStatusLED(2);
                    stethoscope=null;
                    //resetProgrBar();
                    return;
                }

                BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
                if (!mBluetoothAdapter.isEnabled()) {
                    //startActivityForResult(new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE), 0);
                    printConsole("Bluetooth not enabled");
                    //change_StethStatusLED(2);
                    ExecutionStopRequired = true;
                    stethoscope=null;
                    //resetProgrBar();
                    return;
                }
                else {


                    // }

                    IBluetoothManager manager = ConfigurationFactory.getBluetoothManager();
                    Vector<Stethoscope> pairedStethoscopes = manager.getPairedDevices();

                    if (!pairedStethoscopes.isEmpty()) {
                        stethoscope = pairedStethoscopes.get(0);
                        printConsole("Connecting to Stethoscope SN "+stethoscope);
                        stethsn = ""+stethoscope;
                        try {
                            stethoscope.connect();
                        } catch (IOException e) {
                            e.printStackTrace();
                            //Toast.makeText(this, "Error occured in connecting to stethoscope", Toast.LENGTH_LONG).show();
                            //finish();
                            //toast will not work here
                            printConsole("Error occured in connecting to stethoscope");
                            printConsole("Please retry");
                            //change_StethStatusLED(2);
                            ExecutionStopRequired = true;
                            stethoscope=null;
                            //resetProgrBar();
                            return;
                        }
                        printConsole("Successfully connected to Littmann Stethoscope 3200");

                        ExecutionStopRequired = true;
                        //finish();
                        //System.exit(0);
                        //change_StethStatusLED(1);
                        //change_steth_button(2);
                        //resetProgrBar();

                        stethoscope.addStethoscopeListener(new IStethoscopeListener() {
                            @Override
                            public void mButtonDown(boolean b) {

                                printConsole("The m button has been pressed.");
                            }

                            @Override
                            public void mButtonUp() {

                                printConsole("The m button is up.");

                            }

                            @Override
                            public void plusButtonDown(boolean b) {

                                printConsole("The sound amplification level has been changed to " + stethoscope.getSoundAmplificationLevel() + ".");

                            }

                            @Override
                            public void plusButtonUp() {

                                printConsole("The plus button is up.");

                            }

                            @Override
                            public void minusButtonDown(boolean b) {

                                printConsole("The sound amplification level has been changed to " + stethoscope.getSoundAmplificationLevel() + ".");

                            }

                            @Override
                            public void minusButtonUp() {

                                printConsole("The negative button is up.");

                            }

                            @Override
                            public void filterButtonDown(boolean b) {

                                printConsole("The filter changed to " + stethoscope.getFilter() + ".");

                            }

                            @Override
                            public void filterButtonUp() {

                                printConsole("The filter button is up.");

                            }

                            @Override
                            public void onAndOffButtonDown(boolean b) {

                                printConsole("The on/off button has been pressed.");
                                ExecutionStopRequired = true;
                                System.exit(0);

                            }

                            @Override
                            public void onAndOffButtonUp() {

                                printConsole("The on/off button is up.");

                            }

                            @Override
                            public void lowBatteryLevel() {

                                printConsole("The stethoscope has a low battery level.");
                                printConsole("The current battery level is " + stethoscope.getBatteryLevel() + ".");

                            }

                            @Override
                            public void disconnected() {

                                printConsole("The stethoscope has been disconnected.");
                                //change_steth_button(1);

                            }


                            @Override
                            public void error(Errors errors, String s) {

                                printConsole("A " + errors + " has occurred: " + message);

                            }

                            @Override
                            public void endOfOutputStream() {

                                printConsole("The end of the output stream has been reached.");
                                //stethoscope.stopAudioOutput();
                                //stethoscope.disconnect();

                            }

                            @Override
                            public void endOfInputStream() {

                                printConsole("The end of input stream has been reached.");

                            }

                            @Override
                            public void outOfRange(boolean b) {

                                if (b) {
                                    printConsole("The stethoscope has gone out of range.");
                                } else {
                                    printConsole("The stethoscope has come back into range.");
                                }

                            }

                            @Override
                            public void underrunOrOverrunError(boolean b) {

                                if (b) {
                                    printConsole("The audio is underrun.");
                                } else {
                                    printConsole("The audio is overrun.");
                                }

                            }
                        });
                        return;
                    } else {
                        //Toast.makeText(this, "Error: No paired stethoscopes found", Toast.LENGTH_LONG).show();
                        printConsole("Error: No paired stethoscopes found");
                        ExecutionStopRequired = true;
                        //change_StethStatusLED(2);
                        //resetProgrBar();
                        return;
                    }
                }
            }
        });
        connect_to_steth.start();
        ///////
    }


    private void stop_rtauscultation()
    {
        LOCAL_REC = true;
        if(soc != null) {
            try {
                soc.close();
            } catch (IOException ex) {
                //Logger.getLogger(NurseGUI.class.getName()).log(Level.SEVERE, null, ex);
                printConsole("Server: Error Occured during disconnection");
                printConsole("Details: " + ex);
            }
            //change_NWStatusLED(2);
            printConsole("Server Disconnected.");
            soc = null;
        }
        //change_consultation_button(1);
    }
    public void printConsole(String msg)
    {
        list_displaylog.add(df.format(new Date())+ ": "+msg+"\n");
        Log.d("msg",df.format(new Date())+ ": "+msg);
    }



    @Override
    protected void onResume() {
        super.onResume();

        new Thread(new Runnable() {
            @Override
            public void run() {


                    while (true) {
                        if (THREAD_START)//when read from steth is active then the boolean variable THREAD_START will be true.
                        {
                            runOnUiThread(new Runnable() {
                                //int b_index=0;
                                //int max=5;//size of circular buffer
                                @Override
                                public void run() {

                                        addEntry();

                                }
                            });
                        try {
                            sleep(200);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        }


                    }
            }
        }).start();
    }
    private void setupChart() {
        // disable description text
        stethgraph.getDescription().setEnabled(false);
        stethgraph.setNoDataText("No Data for the Moment");
        // enable touch gestures
        stethgraph.setTouchEnabled(false);
        // if disabled, scaling can be done on x- and y-axis separately
        stethgraph.setPinchZoom(false);
        // enable scaling
        stethgraph.setScaleEnabled(false);
        stethgraph.setDrawGridBackground(false);
        // set an alternative background color
        stethgraph.setBackgroundColor(Color.DKGRAY);
        stethgraph.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);

        //stethgraph.setAutoScaleMinMaxEnabled(true);
    }
    private void setupAxes() {
        //XAxis xl = stethgraph.getXAxis();
        xl.setTextColor(Color.WHITE);
        xl.setDrawGridLines(false);
        xl.setAvoidFirstLastClipping(true);
        //xl.setAxisMinimum(0);
        //xl.setAxisMaximum(12000);
        xl.setEnabled(true);


        YAxis leftAxis = stethgraph.getAxisLeft();
        leftAxis.setTextColor(Color.WHITE);
        leftAxis.setAxisMaximum(6000);
        leftAxis.setAxisMinimum(-6000);
        leftAxis.setDrawGridLines(true);

        YAxis rightAxis = stethgraph.getAxisRight();
        rightAxis.setEnabled(false);
    }
    private void setupData() {
        LineData data = new LineData();
        data.setValueTextColor(Color.WHITE);

        // add empty data
        stethgraph.setData(data);
    }
    private void setLegend() {
        // get the legend (only possible after setting data)
        Legend l = stethgraph.getLegend();

        // modify the legend ...
        l.setForm(Legend.LegendForm.CIRCLE);
        l.setTextColor(Color.WHITE);
    }
    private LineDataSet createSet() {
        LineDataSet set = new LineDataSet(null, "Auscultation");
        set.setAxisDependency(YAxis.AxisDependency.LEFT);
        set.setColors(ColorTemplate.VORDIPLOM_COLORS[0]);
        set.setCircleColor(Color.WHITE);
        set.setLineWidth(2f);
        set.setCircleRadius(4f);
        set.setValueTextColor(Color.WHITE);
        set.setValueTextSize(10f);
        set.setDrawCircles(false);
        // To show values of each point
        set.setDrawValues(true);

        return set;
    }

    private void addEntry() {
        if(aus_data_x_index > 800000)
        {
            Toast.makeText(this, "Graph Plotting is Stopped to save resources. Consultation will continue in the background",
                    Toast.LENGTH_LONG).show();
            return;
        }
        int index=aus_data_y_index;
        int max=aus_data_y_index_rear;
        if (max==0) return;
        LineData data = stethgraph.getData();
        if (data != null) {
            ILineDataSet set = data.getDataSetByIndex(0);

            if (set == null) {
                set = createSet();
                data.addDataSet(set);
            }


                while (index < max) {
                    //data.addEntry(new Entry(set.getEntryCount(), aus_data_y[index++]), 0);
                    //try{

                        data.addEntry(new Entry(aus_data_x_index++, aus_data_y[index++]), 0);

//                    }
//                    catch (Exception e)
//                    {
//                        stethgraph.moveViewToX(0);
//                        aus_data_x_index = 0;
//                        Log.d("mem","mem error");
//
//                    }



                    //data.addEntry(new Entry(xindex, aus_data_y[index++]), 0);
                    //Log.d("setcount",""+set.getEntryCount());
                }

                aus_data_y_index = index;

                // let the chart know it's data has changed
                data.notifyDataChanged();
                stethgraph.notifyDataSetChanged();

                // limit the number of visible entries
                stethgraph.setVisibleXRangeMaximum(12000);
                //stethgraph.moveViewToX(set.getEntryCount());
                stethgraph.moveViewToX(aus_data_x_index);
//                if(set.getEntryCount() > 12000)
//                    {
//                        //Log.d("chk","Set entry count "+set.getEntryCount());
//                        //for(int a=0;a<100;++a)
//                        //clearData();
//                        //set.removeEntry(0);
//                        //Log.d("chk","Set entry count "+set.getEntryCount());
//                    }

//            if(aus_data_x_index > 15000)
//            {
//                //Log.d("chk","Set entry count "+set.getEntryCount());
//                //for(int a=0;a<100;++a)
//                //set.removeEntry(0);
//                //Log.d("chk","Set entry count "+set.getEntryCount());
//                //data.clearValues();
//                stethgraph.moveViewToX(0);
//                aus_data_x_index = 0;
//                //Log.d("mem","mem error");
//            }

        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(com.example.aravindb.tcppatientv4.R.layout.activity_main);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        clientmode = (TextView)findViewById(R.id.client);
        LOCAL_REC = false;
        //*************************************
        stethoscope=null;
        buffer = new byte[8000];
        aus_data_y = new float[4000*60*1];//this array will reset itself
        aus_data_y_index=0;
        aus_data_y_index_rear=0;
        ExecutionStopRequired = false;
        THREAD_START = false;
        maxSize=5;
        front = 0;
        rear = 0;
        cir_buf = new byte[5][8000];
        samples_uploaded=0;
        samples_read=0;
        upload_err=0;
        stethreaderr=0;
        soc=null;
        df = new SimpleDateFormat("HH:mm:ss");

        progressDialog = new ProgressDialog(MainActivity.this);
        progressDialog.setMessage("Connecting to Steth....");
        progressDialog.show();
        HR_THREAD_FLAG = true;
        tsrobot();
        //*************************************
        stethgraph = (LineChart) findViewById(com.example.aravindb.tcppatientv4.R.id.chart);
        xl = stethgraph.getXAxis();
        list_displaylog = new ArrayList<String>();
        setupChart();
        setupAxes();
        setupData();
        setLegend();

        /*new AlertDialog.Builder(this)
                .setTitle("Attention Required")
                .setMessage("Whether the Bluetooth Icon in Littmann Stethoscope 3200 is blinking?")
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int whichButton) {
                        openClientChooser();
                        //Toast.makeText(MainActivity.this, "Connecting to Littmann Stethoscope....", Toast.LENGTH_SHORT).show();
                        //tsrobot();
                    }})
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int whichButton) {
                        Toast.makeText(MainActivity.this, "Thank you for the response", Toast.LENGTH_SHORT).show();
                        System.exit(0);
                    }}).show();
*/


    }

    @Override
    public void onBackPressed() {
        System.exit(0);

    }

    public void openClientChooser() {
        modedialog = new Dialog(this); // Context, this, etc.
        modedialog.setContentView(R.layout.modeselector);
        modedialog.setTitle("Choose The Mode");
        modedialog.show();
    }

    private boolean waitForATime(int t) {
        ExecutionStopRequired = false;
        long StartTime = System.currentTimeMillis();
        while(((int) ((System.currentTimeMillis()-StartTime)/1000))<t && !ExecutionStopRequired) {
            try {
                sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
                printConsole("Error in waitForATime method "+e);
            }
        }
        return true;

    }

    private void chkStethAndNWStatus()
    {
        Thread chkThread = new Thread() {
            public void run() {
                while (stethoscope.isConnected() && NetworkConnectivity.checkInternetConnection(MainActivity.this) && !LOCAL_REC) {
                    try {
                        sleep(2000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                if(!LOCAL_REC)
                {
                    if(NetworkConnectivity.checkInternetConnection(MainActivity.this))
                    {
                        printConsole("Stethoscope got disconnected. Pleases restart the stethoscope and retry.");
                        if(soc != null) stop_rtauscultation();

                    }
                    else {

                        printConsole("No Active Internet Connection.");
                        if(soc != null) stop_rtauscultation();

                    }
                    THREAD_START = false;
                    startActivityFromMainThread();
                }
            }};
        chkThread.start();
    }

    private void tsrobot()
    {
        hrThread = new Thread() {
            public void run() {
                while (true){
                    while (HR_THREAD_FLAG == true){
                        start_connect();
                        waitForATime(5);


                        if (stethoscope != null){
                            if(stethoscope.isConnected())
                            {

                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        progressDialog.setMessage("Connecting to Server");
                                        progressDialog.show();
                                    }
                                });
                                //callTomcat();
                                consultation();
                                waitForATime(5);
                                if (soc != null) {

                                    progressDialog.dismiss();
                                    ReadFromSteth.TO_READ_FROM_STETH = true;
                                    chkStethAndNWStatus();
                                    waitForATime(Max_Consultation_Time_in_mins * 60);
                                    if (!ExecutionStopRequired) {
                                        printConsole("Max Allowable Consultation time expires.");
                                    }
                                    stop_rtauscultation();
                                } else {
                                    progressDialog.dismiss();
                                    HR_THREAD_FLAG = false;
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            new AlertDialog.Builder(MainActivity.this)
                                                    .setTitle("Failed")
                                                    .setMessage("Connection to server failed")
                                                    .setIcon(android.R.drawable.ic_dialog_alert)
                                                    .setPositiveButton("Retry", new DialogInterface.OnClickListener() {

                                                        public void onClick(DialogInterface dialog, int whichButton) {

                                                            HR_THREAD_FLAG = true;
                                                            progressDialog = new ProgressDialog(MainActivity.this);
                                                            progressDialog.setMessage("Connecting to Server....");
                                                            progressDialog.show();
                                                        }}).show();
                                        }
                                    });

                                    THREAD_START = false;//to stop the reading thread from steth
                                    //stethgraph.clear();
                                    stethoscope.disconnect();
                                    printConsole("Server connection failed");
                                    if (NetworkConnectivity.checkInternetConnection(MainActivity.this) == false) {
                                        printConsole("No Active Internet Connection...");

                                    }
                                }
                                //Intent intent = new Intent(".MENU");
                                //startActivity(intent);
                            }
                            else
                            {
                                progressDialog.dismiss();
                                HR_THREAD_FLAG = false;
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        new AlertDialog.Builder(MainActivity.this)
                                                .setTitle("Failed")
                                                .setMessage("Connection to steth failed")
                                                .setIcon(android.R.drawable.ic_dialog_alert)
                                                .setPositiveButton("Retry", new DialogInterface.OnClickListener() {

                                                    public void onClick(DialogInterface dialog, int whichButton) {
                                                        //Toast.makeText(MainActivity.this, "Clickkk", Toast.LENGTH_SHORT).show();
                                                        HR_THREAD_FLAG = true;
                                                        //progressDialog = new ProgressDialog(MainActivity.this);
                                                        progressDialog.setMessage("Connecting to Steth....");
                                                        progressDialog.show();
                                                    }}).show();
                                    }
                                });

                                printConsole("Error: Please reconnect steth");
                                printConsole("Please ensure the below conditions for successful consultation (a) A paired Litmmann Stethoscope 3200 (b) Stethoscope should be in connect mode. To make it connect mode press the M button twice. A blinking bluetooth icon indicates connect mode (c) If error persists, please restart the stethoscope and repeat the above steps.");


                            }
                        }
                        else  {

                            /*.setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {

                                public void onClick(DialogInterface dialog, int whichButton) {
                                    Toast.makeText(MainActivity.this, "Thank you for the response", Toast.LENGTH_SHORT).show();
                                    System.exit(0);
                                }}).show();*/
                            printConsole("Error: Please reconnect steth");
                            printConsole("Please ensure the below conditions for successful consultation (a) A paired Litmmann Stethoscope 3200 (b) Stethoscope should be in connect mode. To make it connect mode press the M button twice. A blinking bluetooth icon indicates connect mode (c) If error persists, please restart the stethoscope and repeat the above steps.");

                        }


                        //startActivityFromMainThread();
                    }
                }


            }
        };
        hrThread.start();

    }

    private void drrobot()
    {
       drThread = new Thread() {
            public void run() {

                start_connect();
                waitForATime(5);


                if (stethoscope != null){
                    if(stethoscope.isConnected())
                    {
                        doctorFunction();
                        waitForATime(4);
                        if (soc != null) {
                            chkStethAndNWStatus();
                            waitForATime(Max_Consultation_Time_in_mins * 60);
                            if (!ExecutionStopRequired) {
                                printConsole("Max Allowable Consultation time expires.");
                            }
                            stop_rtauscultation();
                        } else {
                            THREAD_START = false;//to stop the reading thread from steth
                            //stethgraph.clear();
                            stethoscope.disconnect();
                            printConsole("Server connection failed");
                            if (NetworkConnectivity.checkInternetConnection(MainActivity.this) == false) {
                                printConsole("No Active Internet Connection...");

                            }
                        }
                        //Intent intent = new Intent(".MENU");
                        //startActivity(intent);
                    }
                    else
                    {
                        printConsole("Error: Please reconnect steth");
                        printConsole("Please ensure the below conditions for successful consultation (a) A paired Litmmann Stethoscope 3200 (b) Stethoscope should be in connect mode. To make it connect mode press the M button twice. A blinking bluetooth icon indicates connect mode (c) If error persists, please restart the stethoscope and repeat the above steps.");


                    }
                }
                else  {
                    printConsole("Error: Please reconnect steth");
                    printConsole("Please ensure the below conditions for successful consultation (a) A paired Litmmann Stethoscope 3200 (b) Stethoscope should be in connect mode. To make it connect mode press the M button twice. A blinking bluetooth icon indicates connect mode (c) If error persists, please restart the stethoscope and repeat the above steps.");

                }


                startActivityFromMainThread();
            }
        };
        drThread.start();

    }

    public void startActivityFromMainThread(){

        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent (MainActivity.this, LogDisplay.class);
                startActivity(intent);
            }
        });
    }

    private void callTomcat()
    {
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("serverjar", ServerJarName);
        params.put("application","TS");
        params.put("stethoscopesn",stethsn);
        client.post(TomCatServerURL, params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(String response) {

                printConsole("TCP server successfully started");
                consultation();

//                if (response.equals("F")) {
//                    Log.d("BService", "Error occured in cloud DB");
//                } else {
//
//                }
            }

            @Override
            public void onFailure(int statusCode, Throwable error, String content) {
                // TODO Auto-generated method stub
                ExecutionStopRequired = true;
                if (statusCode == 404) {
                    printConsole("Server Error: Requested resource not found");
                } else if (statusCode == 500) {
                    printConsole("Server Error: Something went wrong at server end");
                } else {
                    printConsole("Server Error: Unexpected Error occcured! [Most common Error: Device might not be connected to Internet]");
                }
            }
        });

    }

    private void consultation()
    {


        stethoscope.startAudioInput(AudioInputSwitch.AfterFilter);
        ReadFromSteth m1=new ReadFromSteth();
        final Thread t1 =new Thread(m1);
        t1.start();
        Thread TCPWrThread = new Thread() {
            public void run() {
                //printConsole("button pressed");
                try {
                    printConsole("Locating Server @ "+ServerIP+" socket "+ServerSocket);
                    //printConsole("Locating Server @ "+"192.168.31.200"+" socket "+"1234");
                    //change_NWStatusLED(0);
                    soc = new Socket(ServerIP, ServerSocket) ;
                    //soc = new Socket("192.168.31.200", 1234) ;
                }catch(Exception e)
                {
                    printConsole("Network Error..");
                    printConsole("Details: "+e);
                    ExecutionStopRequired = true;
                    //change_NWStatusLED(2);
                    return;
                }
                //change_NWStatusLED(1);
                printConsole("Server Connected..");
                ExecutionStopRequired = true;
                //change_consultation_button(2);

                StartTime = System.currentTimeMillis();
                printConsole("Consultaion Started");
                LOCAL_REC=false;
                samples_uploaded=0;
                while(!LOCAL_REC)
                {
                    //update_time((int) ((System.currentTimeMillis()-StartTime)/1000)+" secs");
                     // Elapsed time display
                    //jLabel11.setText(""+stethoscope.getBytesReadPerSecond());
                    //jLabel2.setText(""+(samples_uploaded-upload_err)); //Samples Uploaded display
                    if(samples_uploaded<samples_read)
                    {
                        try
                        {

                            //out.write(cir_buf[rear], 0, cir_buf[rear].length);//writing to local file
                            soc.getOutputStream().write(cir_buf[rear], 0, cir_buf[rear].length);//writing to TCP socket

                            //plot(cir_buf[rear]);
                            //change_NWStatusLED(1);

                        }
                        catch(Exception e)
                        {
                            printConsole("Writing error: "+e);
                            //change_NWStatusLED(2);
                            ++upload_err;

                        }
                        if(++rear==maxSize) rear =0;
                        ++samples_uploaded;


                    }


                }

                printConsole("STOP Button Pressed...Consultation Stopped\n Sesssion Duration "+(int)((System.currentTimeMillis()-StartTime)/1000)+" secs"
                );
                //stethosocpe_console_print("Session Duration : "+duration+" secs");
                //printConsole("Stethoscope Read Error "+stethreaderr);
                //printConsole("Upload Error "+upload_err);
                //printConsole("Samples Uploaded "+(samples_uploaded-upload_err));
                //printConsole("Samples Read from stethoscope "+samples_read);
                THREAD_START = false;
                try {
                    t1.join(250);
                } catch (InterruptedException ex) {
                    //Logger.getLogger(NurseGUI.class.getName()).log(Level.SEVERE, null, ex);
                }
                //stethoscope.disconnect();
                //set_stethosocpe_status("disconnected");
                ExecutionStopRequired = true;
                return;
            }
        };
        TCPWrThread.start();
    }

    private void doctorFunction()
    {

        //stethoscope.startAudioInput(AudioInputSwitch.AfterFilter);
        //stethoscope.startAudioOutput();
        stethoscope.setIsFilterButtonEnabled(false);
        stethoscope.startAudioOutput(AudioOutputSwitch.AfterFilter, HeadsetAudioSource.Computer);
        //ReadFromSteth m1=new ReadFromSteth();
        //final Thread t1 =new Thread(m1);
        //t1.start();
        THREAD_START=true;
        Thread TCPRdThread = new Thread() {
            public void run() {
                //printConsole("button pressed");
                try {
                    //printConsole("Locating Server @ "+"192.168."+" socket "+"1234");
                    //change_NWStatusLED(0);
                    soc = new Socket(ServerIP, ServerSocket) ;
                }catch(Exception e)
                {
                    //printConsole("Network Error..");
                    //printConsole("Details: "+e);
                    printConsole("Patient is offline. Please wait and retry");
                    ExecutionStopRequired = true;
                    //change_NWStatusLED(2);
                    return;
                }
                //change_NWStatusLED(1);
                printConsole("Server Connected..");
                ExecutionStopRequired = true;
                //change_consultation_button(2);

                //StartTime = System.currentTimeMillis();
                printConsole("Consultation Begins....");
                LOCAL_REC=false;
                //samples_uploaded=0;
                int MSB=0;
                int LSB=0;
                int temp1=0;
                while(!LOCAL_REC)
                {
                    //update_time((int) ((System.currentTimeMillis()-StartTime)/1000)+" secs");
                    // Elapsed time display
                    //jLabel11.setText(""+stethoscope.getBytesReadPerSecond());
                    //jLabel2.setText(""+(samples_uploaded-upload_err)); //Samples Uploaded display
                    byte [] b= new byte[8000];
                    int read1= 0;
                    try {
                        read1 = soc.getInputStream().read(b);
                        if(read1>0)
                        {
                            stethoscope.getAudioOutputStream().write(b, 0, read1);
                            for (int k = 0; k < read1; )
                            {
                                LSB = b[k++];//one byte
                                MSB = b[k++];//one byte
                                //b+=(8000.0/samples_taken);
                                //b+=40;
                                temp1 = ((MSB << 8) | (LSB & 0x0FF));
                                if ((temp1 & 0x8000) == 0x8000) {
                                    //if sign bit is one ie value is negative.
                                    //taking 2's compliment of the value(excluding sign bit)
                                    temp1 = ~temp1;
                                    temp1 &= 0x7FFF;
                                    temp1++;
                                    temp1 = -1 * temp1;
                                }
                                aus_data_y[aus_data_y_index_rear] = temp1;

                                //So the aus_data_y array will reset to initial position on reaching the limits.
                                if(++aus_data_y_index_rear == aus_data_y.length) {
                                    aus_data_y_index_rear = 0;
                                    aus_data_y_index=0;
                                }
                            }

                        }
                        //sleep(500);
                    }
                    catch (Exception e) {
                        e.printStackTrace();
                        printConsole("Exception "+e);
                        ExecutionStopRequired = true;
                    }
                    /*if(samples_uploaded<samples_read)
                    {
                        try
                        {

                            //out.write(cir_buf[rear], 0, cir_buf[rear].length);//writing to local file
                            soc.getOutputStream().write(cir_buf[rear], 0, cir_buf[rear].length);//writing to TCP socket
                            //plot(cir_buf[rear]);
                            //change_NWStatusLED(1);

                        }
                        catch(Exception e)
                        {
                            printConsole("Writing error: "+e);
                            //change_NWStatusLED(2);
                            ++upload_err;

                        }
                        if(++rear==maxSize) rear =0;
                        ++samples_uploaded;


                    }*/


                }
                printConsole("STOP Button Pressed...Consultation Stopped");
                //stethosocpe_console_print("Session Duration : "+duration+" secs");
                //printConsole("Stethoscope Read Error "+stethreaderr);
                //printConsole("Upload Error "+upload_err);
                //printConsole("Samples Uploaded "+(samples_uploaded-upload_err));
                //printConsole("Samples Read from stethoscope "+samples_read);
                ExecutionStopRequired = true;
                THREAD_START = false;

                //stethoscope.disconnect();
                //set_stethosocpe_status("disconnected");
                return;
            }
        };
        TCPRdThread.start();

    }

    public void patientmode(View view) {
        clientmode.setText("PATIENT MODE");
        Toast.makeText(MainActivity.this, "Connecting to Littmann Stethoscope....", Toast.LENGTH_SHORT).show();
        tsrobot();
        modedialog.dismiss();
    }

    public void doctormode(View view) {

        clientmode.setText("DOCTOR MODE");
        Toast.makeText(MainActivity.this, "Connecting to Littmann Stethoscope....", Toast.LENGTH_SHORT).show();
        drrobot();
        modedialog.dismiss();
    }
}


