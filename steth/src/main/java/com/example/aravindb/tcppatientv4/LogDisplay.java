package com.example.aravindb.tcppatientv4;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import static com.example.aravindb.tcppatientv4.MainActivity.list_displaylog;
import static com.example.aravindb.tcppatientv4.MainActivity.logarrayAdapter;

public class LogDisplay extends AppCompatActivity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_display);
        ListView lv = (ListView)findViewById(com.example.aravindb.tcppatientv4.R.id.displaylog);
        //logarrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_expandable_list_item_1, list_displaylog);
        logarrayAdapter = new ArrayAdapter<String>
                (this, android.R.layout.simple_expandable_list_item_1, list_displaylog){
            @Override
            public View getView(int position, View convertView, ViewGroup parent){
                // Get the current item from ListView
                View view = super.getView(position,convertView,parent);

                // Get the Layout Parameters for ListView Current Item View
                ViewGroup.LayoutParams params = view.getLayoutParams();

                // Set the height of the Item View
                params.height = 100;
                view.setLayoutParams(params);

                return view;
            }
        };
        lv.setAdapter(logarrayAdapter);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position,
                                    long id) {

                String item = ((TextView)view).getText().toString();

                Toast.makeText(getBaseContext(), item, Toast.LENGTH_LONG).show();

            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //list_displaylog.clear();
        System.exit(0);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        System.exit(0);
    }

    public void retry(View view) {
        //Intent intent = new Intent (LogDisplay.this, MainActivity.class);
        //startActivity(intent);
        Intent i = getBaseContext().getPackageManager()
                .getLaunchIntentForPackage( getBaseContext().getPackageName() );
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(i);
    }
}
