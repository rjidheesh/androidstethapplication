package com.example.aravindb.tcppatientv4;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.util.Log;

/**
 * Created by user on 2/6/17.
 */

public  class NetworkConnectivity {
    public static boolean checkInternetConnection(Context ctx) {
        if (ctx == null)
            return false;

        ConnectivityManager cm =
                (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
           checkNetworkSpeed(ctx);
            return true;
        }
        return false;
    }

    public static  int checkNetworkSpeed(Context ctx){
        WifiManager wifiManger = (WifiManager) ctx.getSystemService(Context.WIFI_SERVICE);
        WifiInfo wifiInfo = wifiManger.getConnectionInfo();
        int speedMbps = wifiInfo.getLinkSpeed();
        Log.d("ccc", "checkInternetConnection: ="+speedMbps);
        return speedMbps;
    }
}
