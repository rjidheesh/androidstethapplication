/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.aravindb.tcppatientv4;

import static com.example.aravindb.tcppatientv4.MainActivity.aus_data_y_index_rear;
import static com.example.aravindb.tcppatientv4.MainActivity.aus_data_y_index;
import static com.example.aravindb.tcppatientv4.MainActivity.THREAD_START;
import static com.example.aravindb.tcppatientv4.MainActivity.buffer;
import static com.example.aravindb.tcppatientv4.MainActivity.cir_buf;
import static com.example.aravindb.tcppatientv4.MainActivity.aus_data_y;
import static com.example.aravindb.tcppatientv4.MainActivity.maxSize;
import static java.lang.Thread.sleep;
import java.util.logging.Level;
import java.util.logging.Logger;
import static com.example.aravindb.tcppatientv4.MainActivity.stethoscope;
import static com.example.aravindb.tcppatientv4.MainActivity.front;
import static com.example.aravindb.tcppatientv4.MainActivity.samples_read;
import static com.example.aravindb.tcppatientv4.MainActivity.stethreaderr;
/**
 *
 * @author Aravind
 */
public class ReadFromSteth implements Runnable {
    public static boolean TO_READ_FROM_STETH = false;
    @Override
    public void run() {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        
        /**
         * This thread will read the raw audio stream from stethosocpe
         * and write the same to a circular buffer.
         */
            int MSB=0;
            int LSB=0;
            int temp1=0;

            //new MainActivity().printConsole("Read from Steth begins...");
            aus_data_y_index_rear=0;
            aus_data_y_index=0;
            samples_read =0;
            front=0;
            while (!TO_READ_FROM_STETH);
            THREAD_START = true;
            stethreaderr=0;
            long Start_t=System.currentTimeMillis();
        while(THREAD_START)
           {
        try {
            //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            //System.out.println("thread is running...");

            int read = stethoscope.getAudioInputStream().read(buffer, 0, buffer.length);
            if(read>0) {
                //duration = (int) ((System.currentTimeMillis()-Start_t)/1000);
                cir_buf[front] = buffer;

                //int b=0;
                for (int b = 0; b < 8000; )
                {
                    LSB = buffer[b++];//one byte
                    MSB = buffer[b++];//one byte
                    //b+=(8000.0/samples_taken);
                    //b+=40;
                    temp1 = ((MSB << 8) | (LSB & 0x0FF));
                    if ((temp1 & 0x8000) == 0x8000) {
                        //if sign bit is one ie value is negative.
                        //taking 2's compliment of the value(excluding sign bit)
                    temp1 = ~temp1;
                    temp1 &= 0x7FFF;
                    temp1++;
                    temp1 = -1 * temp1;
                    }
                    aus_data_y[aus_data_y_index_rear] = temp1;

                    //So the aus_data_y array will reset to initial position on reaching the limits.
                    if(++aus_data_y_index_rear == aus_data_y.length) {
                        aus_data_y_index_rear = 0;
                        aus_data_y_index=0;
                    }
                }
                    //cir_time[front]=duration;
                 //Entry pt = new Entry(duration, 100000f);
                //vals.add(pt);
                if(++front == maxSize) front =0;
                ++samples_read;
 
            }
            else 
                sleep(200);
            } 
        catch (Exception ex) 
            {
            Logger.getLogger(ReadFromSteth.class.getName()).log(Level.SEVERE, null, ex);
            ++stethreaderr;
            }
              
           }
    }
    
}
